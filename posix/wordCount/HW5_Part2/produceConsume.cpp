/* 
 * File:   main.cpp
 * Author: frank
 *
 * Created on April 3, 2014, 8:53 AM
 */

#include <cstdlib>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdbool.h>

typedef struct {
    int* buffer; // Slots to produce/consumer, i.e. buffer 
    int  bufferSize; //Size of our buffer
    int* in;         // buffer[inCount%BUFFER_SIZE] is the next empty slot
    int* out; // buffer[outCount%BUFFER_SIZE] is the next full slot
    int numToProduceConsume; // number for each thread to produce or consume.
    int threadID; //Thread ID
} threadData;

//GLOBAL definition of semaphores!
sem_t full; //Number of full slots
sem_t empty; //Number of empty slots
pthread_mutex_t mutexBuffer; //Enforce mutual exclusion on shared data

using namespace std;

//Forward declarations
void* consumerThread(void *t);
void* producerThread(void *t);

/*
 * 
 */
int main(int argc, char** argv) {

    //Runtime parameters retrieved from the command line.
    int numProducers = 8;
    int numConsumers = 8;
    int numItemsToProduce = 8;
    int bufferSize = 8;
    int i = 0;

    //Process command line
    bool hitProducer = false; // "-p" option sets number of producers
    bool hitConsumer = false; // "-c" option sets number of consumers
    bool hitBuffer = false;   // "-b" option sets size of buffer
    bool hitNumItems = false; // "-i" option sets number of items to produce/consume

    if (argc == 1)
    {
        printf("HELP: produceConsume \n\t[-p] [num producers] \n\t[-c] [num consumers] \n\t[-b] [num buffers]\n\t[-i] [num items to produce]\n");
        return 0;
    }

    for (i = 1; i < argc; i++)  /* Skip argv[0] (program name). */
    {
        //First check to see if boolean flags are set, if so set the appropriate value.
        if (hitProducer)
        {
            numProducers = atoi(argv[i]);
            hitProducer = false;
        }
        else if (hitConsumer)
        {
            numConsumers = atoi(argv[i]);
            hitConsumer = false;
        }
        else if (hitBuffer)
        {
            bufferSize = atoi(argv[i]);
            hitBuffer = false;
        }
        else if (hitNumItems)
        {
            numItemsToProduce = atoi(argv[i]);
            hitNumItems = false;
        }
        
        /*
         * Use the 'strcmp' function to compare the argv values
        */
        if (strcmp(argv[i], "-p") == 0)  /* Process optional arguments. */
        {
            hitProducer = true;  /* This is used as a boolean value. */
        }
        else if (strcmp(argv[i], "-c") == 0)
        {
            hitConsumer = true;
        }
        else if (strcmp(argv[i], "-b") == 0)
        {
            hitBuffer = true;
        }
        else if (strcmp(argv[i], "-i") == 0)
        {
            hitNumItems = true;
        }
        else
        {
            printf("Unknown command line option entered!\n");
        }
        
        
    }

    printf("Running Produce/Consume with following parameters\n");
    printf("Buffer Size: %d\n", bufferSize);
    printf("Number of items per thread to produce/consume: %d\n", numItemsToProduce);
    printf("Number of consumers: %d\n", numConsumers);
    printf("Number of producers: %d\n", numProducers);

    
    //Initialize shared data that will be passed into each thread
    //Our pthread handles
    pthread_t producerThreads[numProducers];
    pthread_t consumerThreads[numConsumers];

    //pthread attr
    pthread_attr_t attr;
    /* Initialize and set thread detached attribute */
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    //void* status for return value
    void *status;
    
    int inPos = 0;
    int outPos = 0;
    pthread_mutex_t mutexBuffer = PTHREAD_MUTEX_INITIALIZER;

    //Initialize our semaphores
    
    sem_init(&full, 0, 0);
    sem_init(&empty, 0, bufferSize);
    int* buffer = (int*) malloc(sizeof (int) * bufferSize);

    //Spawn producers
    for (i = 1; i <= numProducers; i++) {
        //Setup our threadData struct to point to shared data
        threadData* newProducerData = (threadData*) malloc(sizeof (threadData) * 1);
        newProducerData->buffer = buffer;
        newProducerData->bufferSize = bufferSize;
        newProducerData->in = &inPos;
        newProducerData->out = &outPos;
        newProducerData->threadID = i;
        newProducerData->numToProduceConsume = numItemsToProduce;

        //Create the new producer thread
        int rc = 0;
        rc = pthread_create(&producerThreads[i-1], &attr, producerThread, (void *) newProducerData);

        if (rc) {
            printf("ERROR; return code from pthread_create() is %d\n", rc);
            exit(-1);
        }
    }

    //Spawn consumers
    for (i = 1; i <= numConsumers; i++) {
        //Setup our threadData struct to point to shared data
        threadData* newConsumerData = (threadData*) malloc(sizeof (threadData) * 1);
        newConsumerData->buffer = buffer;
        newConsumerData->bufferSize = bufferSize;
        newConsumerData->in = &inPos;
        newConsumerData->out = &outPos;
        newConsumerData->threadID = i;
        newConsumerData->numToProduceConsume = numItemsToProduce;

        //Create the new producer thread
        int rc = 0;
        rc = pthread_create(&consumerThreads[i-1], &attr, consumerThread, (void *) newConsumerData);

        if (rc) {
            printf("ERROR; return code from pthread_create() is %d\n", rc);
            exit(-1);
        }
    }


    /* Free attribute and wait for the other threads */
    pthread_attr_destroy(&attr);

    //Wait for producers
    for (i = 0; i < numProducers; i++) {
        int rc = pthread_join(producerThreads[i], &status);
        if (rc) {
            printf("ERROR; return code from pthread_join() is % d\n", rc);
            exit(-1);
        }
    }

    //Wait for consumers
    for (i = 0; i < numConsumers; i++) {
        int rc = pthread_join(consumerThreads[i], &status);
        if (rc) {
            printf("ERROR; return code from pthread_join() is % d\n", rc);
            exit(-1);
        }
    }

    //Finished code here, calculate totals and output them

    printf("Finished Producer/Consumer\n");

    /* Last thing that main() should do */
    pthread_exit(NULL);

    return 0;
}

void* consumerThread(void *t) {
    //Retrieve and cast threadData
    threadData* tData;
    
    int value = 0;
    
    tData = (threadData*) t;

    int i = 0;
    
    for (i = 0; i < tData->numToProduceConsume; i++) {
        //No empty slots, wait
        sem_wait(&full);
        
        /*
        sem_getvalue(&full, &value);
        printf("Full semaphore value: %d\n", value);
        */
        
        //Wait for access to shared buffer
        pthread_mutex_lock(&mutexBuffer);
        
        //We have the mutex so read the data
        
        //retrieve buffer data to consume
        int item = tData->buffer[*(tData->out)];
        
        *tData->out = ((*tData->out) +1) % tData->bufferSize; //Use modulus to wrap the next spot to write to around
        
        printf("Consumer Thread: %d consumed: %d\n", tData->threadID, item); 
        fflush(stdout);
        
        /* Release the buffer */
        pthread_mutex_unlock(&mutexBuffer);
        /* Increment the number of full slots */
        sem_post(&empty);
        
        /*
        sem_getvalue(&empty, &value);
        printf("Empty semaphore value: %d\n", value);
        */
    }

    return NULL;
}

void* producerThread(void *t) {

    //data that is passed in to the thread as a void* ptr
    threadData* tData;

    int value = 0;
    //Cast the pointer
    tData = (threadData*) t;
    int i = 0;

    for (i = 0; i < tData->numToProduceConsume; i++) {
        //No empty slots, wait
        sem_wait(&empty);
        
        /*
        sem_getvalue(&empty, &value);
        printf("Empty semaphore value: %d\n", value);
        */
          
        //Wait for access to shared buffer
        pthread_mutex_lock(&mutexBuffer);
        
        //thread_number * 1000000 + counter
        int item = tData->threadID * 1000000 + i;
        
        //We have the mutex so write the data
        tData->buffer[*(tData->in)] = item; //Store produced item
        *tData->in = ((*tData->in) + 1) % tData->bufferSize; //Use modulus to wrap the next spot to write to around
        
        printf("Producer Thread: %d produced: %d\n", tData->threadID, item);
        fflush(stdout);
        
        /* Release the buffer */
        pthread_mutex_unlock(&mutexBuffer);
        /* Increment the number of full slots */
        
        sem_post(&full);
        
        /*
        sem_getvalue(&full, &value);
        printf("Full semaphore value: %d\n", value);
        */
    }

    return NULL;
}
