/*  Name: Frank Dye
    
    `:w
    Date: 3/12/2014
    Description: A multi-threaded word count program using posix
 */

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>

#define STRING_SIZE 1024


#define BUFFER_SIZE 100 * 1024
#define NUM_THREADS 8

//Open source file as read only, 
#define SOURCE_FLAGS O_RDONLY

//GLOBAL threadWordCount for purposes of illustrating race conditions
int threadWordCount;

//Forward declarations
DWORD WINAPI threadCountWords( LPVOID lpParam);
void wipeBuffer(char* buffer, char symbol, int maxBufferLength);
int findNextWhiteSpace(char* buffer, int startIndex, int maxBufferSize);
void processError(HRESULT errorCode);


//Thread data structure
typedef struct thread_data {
    int threadId;
    int startIndex;
    int stopIndex;
    int* pThreadWordCount;
    char* buffer;
} thread_data;



//Fix this for final touch.
DWORD WINAPI threadCountWords(LPVOID lpParam) {

    //data that is passed in to the thread as a void* ptr
    thread_data *tData;

    //Cast the pointer
    tData = (thread_data*) lpParam;

    //Variable setup
    bool hitSpace = false;

    bool nextSpace = false;
    
	int i = tData->startIndex;
    
    //Does our buffer start on a word or a space
    if (isspace(tData->buffer[i]))
    {
        hitSpace = true;
    }
    else
    {
		*(tData->pThreadWordCount) += 1;
    }
    
    for (; i < tData->stopIndex; i++) {
        nextSpace =  isspace(tData->buffer[i]);
        if (hitSpace == true && nextSpace == false) {
            //If we have hit a space and find a non-space then we have encountered a new word so increment wordCount
            //and set hitSpace to false

			
			*(tData->pThreadWordCount) += 1;
        }
        hitSpace = nextSpace;
    }

	//printf("Thread %ld done. Result = %d\n", tData->threadId, *(tData->pThreadWordCount));
	return 0;
}

//Helper function to process error codes 
void processError(HRESULT errorCode)
{

    if (errorCode == ERROR_FILE_NOT_FOUND)
    {
        printf("Source File Not Found!\n");
        exit(1);
    }
    else if (errorCode == ERROR_FILE_EXISTS)
    {
        printf("Destination File Already Exists!\n");
        exit(1);
    }
    else if (errorCode == ERROR_IO_PENDING)
    {
        printf("IO Error reading/writing!");
        exit(1);
    }
    else if (errorCode == ERROR_SUCCESS)
    {
        //No error code, ignore
    }
    else if (errorCode == S_OK)
    {
        //No error, file opened okay, ignore
    }
    else
    {
        printf("Error in program!\n");
        exit(1);
    }
}

void wipeBuffer(char* buffer, char symbol, int maxBufferLength) {
    int i = 0;
    for (i = 0; i < maxBufferLength; i++) {
        buffer[i] = symbol;
    }
}

int findNextWhiteSpace(char* buffer, int startIndex, int maxBufferSize) {
    int currIndex = startIndex;
    while (!isspace(buffer[currIndex]) && currIndex < maxBufferSize) {
        currIndex++;
    }

    return currIndex;
}

int main(int argc, char** argv) {

    char szBuffer[BUFFER_SIZE];

    //Source file descriptors
    HANDLE fdSourceFile = NULL;

    int threadStartIndex[NUM_THREADS];
    int threadStopIndex[NUM_THREADS];
    
    //Zero global variable
    threadWordCount = 0;
    
    int totalWordCount = 0;

    //Our thread handles
    HANDLE hThreads[NUM_THREADS];
    DWORD  dwThreadIdArray[NUM_THREADS];

    //thread data
    struct thread_data threadData[NUM_THREADS];


    //Open source file
    char szSourceFileName[STRING_SIZE];
    char szCWD[STRING_SIZE];

    int cwdLength = 0;
    int i = 0;
    
    //Acquire the current working directory
    cwdLength = GetCurrentDirectory(STRING_SIZE, szCWD);
    
    sprintf(szSourceFileName, "%s\\%s", szCWD, argv[1]);

    fdSourceFile = CreateFile(szSourceFileName, GENERIC_READ, 0, 
        NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL,
        NULL);

    //Check to see if file was created
    int errorCode = GetLastError();

    processError(errorCode); //exit if failed, otherwise continue.

    int operationStatus;
	operationStatus = false;

    DWORD numBytesRead = 0;

    do {

        //Wipe buffer with space character
        wipeBuffer(szBuffer, ' ', BUFFER_SIZE);

        //Read BUFFER_SIZE bytes from the file
        operationStatus = ReadFile(fdSourceFile, &szBuffer, BUFFER_SIZE, &numBytesRead, NULL);


        if (operationStatus == false) {
            //Error reading from file
            errorCode = GetLastError();

            processError(errorCode);
        }

        int bufferDivider = 0;

        //Our partition of the buffer
        bufferDivider = numBytesRead / NUM_THREADS;

        int tempBufferBoundary = 0;
        int tempStartIndex = 0;


        //Declare our index variables
        int i = 0;
        //Boundaries for Pthreads
        for (i = 0; i < NUM_THREADS; i++) {
            tempBufferBoundary = (i + 1) * bufferDivider;
            threadStartIndex[i] = tempStartIndex;
            tempBufferBoundary = findNextWhiteSpace(szBuffer, tempBufferBoundary, BUFFER_SIZE);
            threadStopIndex[i] = tempBufferBoundary;
            tempStartIndex = threadStopIndex[i] + 1;
        }

        //Setup out threadData struct
        for (i = 0; i < NUM_THREADS; i++) {
            threadData[i].buffer = szBuffer;
            threadData[i].threadId = i;
            threadData[i].startIndex = threadStartIndex[i];
            threadData[i].stopIndex = threadStopIndex[i];
            threadData[i].pThreadWordCount = &threadWordCount;
        }

        //Create out threads and hand them the threadData struct saving their thread handle
        int rc = 0;
        for (i = 0; i < NUM_THREADS; i++) {
            hThreads[i] = CreateThread( 
                NULL,                   // default security attributes
                0,                      // use default stack size  
                threadCountWords,       // thread function name
                (LPVOID*) &threadData[i],          // argument to thread function 
                0,                      // use default creation flags 
                &dwThreadIdArray[i]);   // returns the thread identifier 

            if (hThreads[i] == NULL) 
            {
                printf("Error creating thread %d\n", i);
                ExitProcess(3);
            }
        }

        // Wait until all threads have terminated.

        WaitForMultipleObjects(NUM_THREADS, hThreads, TRUE, INFINITE);

        
        //Add the total to our wordCount variable
        totalWordCount += threadWordCount;      
        

    } while (!(numBytesRead < BUFFER_SIZE));

    //Finished code here, calculate totals and output them

    printf("%d words\n", totalWordCount);
    
   //Close all are threads
    for (int i = 0; i < NUM_THREADS; i++)
    {
        CloseHandle(hThreads[i]);
    }

    /* Last thing that main() should do */
    return 0;
}
