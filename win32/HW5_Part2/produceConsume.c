/* 
 * File:   main.cpp
 * Author: frank
 *
 * Created on April 3, 2014, 8:53 AM
 */

#include <windows.h>
#include <stdio.h>


//Define bool for stupid C90
typedef int bool;
#define true 1
#define false 0


//Open source file as read only
 #define SOURCE_FLAGS O_RDONLY

typedef struct thread_data {
    int* buffer; // Slots to produce/consumer, i.e. buffer 
    int  bufferSize; //Size of our buffer
    int* in;         // buffer[inCount%BUFFER_SIZE] is the next empty slot
    int* out; // buffer[outCount%BUFFER_SIZE] is the next full slot
    int numToProduceConsume; // number for each thread to produce or consume.
    int threadID; //Thread ID
} threadData;

//GLOBAL definition of semaphores!
HANDLE full; //Number of full slots
HANDLE empty; //Number of empty slots
HANDLE mutex; //Enforce mutual exclusion on shared data

//Forward declarations
DWORD WINAPI consumerThread(LPVOID lpParam);
DWORD WINAPI producerThread(LPVOID lpParam);

/*
 * 
 */
int main(int argc, char** argv) {

    //Runtime parameters retrieved from the command line.
    int numProducers = 8;
    int numConsumers = 8;
    int numItemsToProduce = 8;
    int bufferSize = 8;
    int i = 0;

    //Process command line
    bool hitProducer = false; // "-p" option sets number of producers
    bool hitConsumer = false; // "-c" option sets number of consumers
    bool hitBuffer = false;   // "-b" option sets size of buffer
    bool hitNumItems = false; // "-i" option sets number of items to produce/consume

	if (argc == 1)
	{
		printf("HELP: produceConsume \n\t[-p] [num producers] \n\t[-c] [num consumers] \n\t[-b] [num buffers]\n\t[-i] [num items to produce]\n");
		return 0;
	}

    for (i = 1; i < argc; i++)  /* Skip argv[0] (program name). */
    {
        //First check to see if boolean flags are set, if so set the appropriate value.
        if (hitProducer)
        {
            numProducers = atoi(argv[i]);
			hitProducer = false;
        }
        else if (hitConsumer)
        {
            numConsumers = atoi(argv[i]);
			hitConsumer = false;
        }
        else if (hitBuffer)
        {
            bufferSize = atoi(argv[i]);
			hitBuffer = false;
        }
        else if (hitNumItems)
        {
            numItemsToProduce = atoi(argv[i]);
			hitNumItems = false;
        }
        
        /*
         * Use the 'strcmp' function to compare the argv values
        */
        if (strcmp(argv[i], "-p") == 0)  /* Process optional arguments. */
        {
            hitProducer = true;  /* This is used as a boolean value. */
        }
        else if (strcmp(argv[i], "-c") == 0)
        {
            hitConsumer = true;
        }
        else if (strcmp(argv[i], "-b") == 0)
        {
            hitBuffer = true;
        }
        else if (strcmp(argv[i], "-i") == 0)
        {
            hitNumItems = true;
        }
        
        
    }

	printf("Running Produce/Consume with following parameters\n");
	printf("Buffer Size: %d\n", bufferSize);
	printf("Number of items per thread to produce/consume: %d\n", numItemsToProduce);
	printf("Number of consumers: %d\n", numConsumers);
	printf("Number of producers: %d\n", numProducers);
    
    //Initialize shared data that will be passed into each thread
    //Our pthread handles
    int totalThreads = 0;
    totalThreads = numProducers + numConsumers;
    HANDLE* threads = (HANDLE*) malloc(sizeof(HANDLE) * totalThreads);
    DWORD* threadID = (DWORD*) malloc(sizeof(DWORD) * totalThreads);
    
    int inPos = 0;
    int outPos = 0;
    
    //Initialize mutex
    mutex = CreateMutex(
        NULL,
        FALSE,
        NULL);

    if (mutex == NULL)
    {
        printf("CreateMutex error: %d\n", GetLastError());
    }

    //Initialize our semaphores
    full = CreateSemaphore( 
        NULL,           // default security attributes
        0,  // initial count
        bufferSize,  // maximum count
        NULL);          // unnamed semaphore

    if (full == NULL) 
    {
        printf("CreateSemaphore full error: %d\n", GetLastError());
        return 1;
    }

    empty = CreateSemaphore( 
        NULL,           // default security attributes
        bufferSize,  // initial count
        bufferSize,  // maximum count
        NULL);          // unnamed semaphore

    if (empty == NULL) 
    {
        printf("CreateSemaphore empty error: %d\n", GetLastError());
        return 1;
    }

    //Creeate our consumer/producer buffer
    int* buffer = (int*) malloc(sizeof (int) * bufferSize);

    //Spawn producers
    for (i = 0; i < numProducers; i++) {
        //Thread Data
        threadData* newProducerData;

        newProducerData = (threadData*) malloc(sizeof(threadData) * 1);

        //Setup our threadData struct to point to shared data
        newProducerData->buffer = buffer;
        newProducerData->bufferSize = bufferSize;
        newProducerData->in = &inPos;
        newProducerData->out = &outPos;
        newProducerData->threadID = i+1;
        newProducerData->numToProduceConsume = numItemsToProduce;

        //Create the new producer thread
        threads[i] = CreateThread(
            NULL,       // default security attributes
            0,          // use default stack size
            producerThread, //thread function name
            (LPVOID*) newProducerData,  //argument to thread
            0,          // use default creation flags
            &threadID[i]); //returns the thread identifier

        if (threads[i] == NULL)
        {
            printf("Error creating thread &d\n", i);
            ExitProcess(3);
        }
    }

    //Spawn consumers
    for (i = numProducers; i < totalThreads; i++) {
        //Thread Data
        threadData* newConsumerData;

        newConsumerData = (threadData*) malloc(sizeof(threadData) * 1);

        //Setup our threadData struct to point to shared data
        newConsumerData->buffer = buffer;
        newConsumerData->bufferSize = bufferSize;
        newConsumerData->in = &inPos;
        newConsumerData->out = &outPos;
        newConsumerData->threadID = i+1;
        newConsumerData->numToProduceConsume = numItemsToProduce;

        //Create the new producer thread
        threads[i] = CreateThread(
            NULL,       // default security attributes
            0,          //use default stack size
            consumerThread, //thread function name
            (LPVOID*) newConsumerData,  //argument to thread
            0,          // use default creation flags
            &threadID[i]);   //returns the thread identifier

        if (threads[i] == NULL)
        {
            printf("Error creating thread &d\n", i);
            ExitProcess(3);
        }
    }

    //
    WaitForMultipleObjects(totalThreads,
                         threads,
                         TRUE, 
                         INFINITE);


    //Finished code here, calculate totals and output them

    printf("Finished Producer/Consumer\n");

    //Close all are threads
    for (int i = 0; i < totalThreads; i++)
    {
        CloseHandle(threads[i]);
    }

    /* Last thing that main() should do */
    CloseHandle(full);
    CloseHandle(empty);
    CloseHandle(mutex);

    //Delete buffer
    free(buffer);

    return 0;
}

DWORD WINAPI consumerThread(LPVOID lpParam) {
    //Retrieve and cast threadData
    threadData* tData;
    
    int value = 0;
    
    tData = (threadData*) lpParam;

    int i = 0;
    
    for (i = 0; i < tData->numToProduceConsume; i++) {
        //Grab empty semaphore
        WaitForSingleObject( 
            full,                // handle to semaphore
            INFINITE);           // zero-second time-out interval
        
        //Grab mutex
        WaitForSingleObject( 
            mutex,    // handle to mutex
            INFINITE);  // no time-out interval
        
        //We have the mutex so read the data
        
        //retrieve buffer data to consume
        int item = tData->buffer[*(tData->out)];
        
        *tData->out = ((*tData->out) +1) % tData->bufferSize; //Use modulus to wrap the next spot to write to around
        
        printf("Consumer Thread: %d consumed: %d\n", tData->threadID, item); 
        fflush(stdout);
        
        //Release mutex
        ReleaseMutex(mutex);
        
        ReleaseSemaphore(empty,  // handle to semaphore
                        1,       // increase count by one
                        NULL);    // not interested in previous count
		
    }

    //Free up threadData
    free(tData);

    return NULL;
}

DWORD WINAPI producerThread(LPVOID lpParam) {

    //data that is passed in to the thread as a void* ptr
    threadData* tData;

    int value = 0;
    //Cast the pointer
    tData = (threadData*) lpParam;
    int i = 0;

    for (i = 0; i < tData->numToProduceConsume; i++) {

        //Grab empty semaphore
        WaitForSingleObject( 
            empty,                // handle to semaphore
            INFINITE);           // zero-second time-out interval
        
        //Grab mutex
        WaitForSingleObject( 
            mutex,    // handle to mutex
            INFINITE);  // no time-out interval
        
        //thread_number * 1000000 + counter
        int item = tData->threadID * 1000000 + i;
        
        //We have the mutex so write the data
        tData->buffer[*(tData->in)] = item; //Store produced item
        *tData->in = ((*tData->in) + 1) % tData->bufferSize; //Use modulus to wrap the next spot to write to around
        
        printf("Producer Thread: %d produced: %d\n", tData->threadID, item);
        fflush(stdout);

        //Release mutex
        ReleaseMutex(mutex);
        
        ReleaseSemaphore(full,  // handle to semaphore
                        1,       // increase count by one
                        NULL);    // not interested in previous count

    }

    return NULL;
}
